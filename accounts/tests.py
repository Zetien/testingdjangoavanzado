from django.test import TestCase, TransactionTestCase
from django.db.utils import IntegrityError
from django.utils.text import slugify

from .models import User
# Create your tests here.

"""
* Probar que el email sea unico. [OK]
* Probar que el atributo 'email', del modelo de usuario sea obligatorio.[OK]
* Probar `first_name`, `last_name` del modelo de usuario sea obligatorio.[OK]
* Se debe autogenerar un `slug` con el primer y segundo nombre de usuario. [OK]
"""


class TestUser(TransactionTestCase):

    def test_slug_autogeneration(self):
        """ """
        user = User(
            username='jaime',
            password='123456',
            email='mimosa@gmail.com',
            first_name='Jaime',
            last_name='Negrete',
        )
        expected_slug = slugify(user.get_full_name())
        user.save()
        db_user = User.objects.get(email=user.email)
        self.assertEqual(db_user.slug, expected_slug)

    def test_user_email_unique(self):
        """"""
        user = User(
            username='jaime',
            password='123456',
            email='mimosa@gmail.com',
            first_name='Jaime',
            last_name='Negrete',
        )
        user.save()
        user_two = User(
            username='zetien',
            password='25984',
            email='mimosa@gmail.com',
            first_name='Jorge',
            last_name='Zetien',
        )
        with self.assertRaisesMessage(IntegrityError, expected_message='UNIQUE constraint failed: accounts_user.email'):
            user_two.save()

    def test_user_email_mandatory(self):
        """
        Testing `accounts.User` model has the `email` column as not-null
        """
        user = User(
            username='jaime',
            password='123456',
            first_name='Jorge',
            last_name='Zetien',
        )
        with self.assertRaises(IntegrityError):
            user.save()
        bad_values = [
            '',
            '    ',
            '@masfaf',
            'jaime@',
            'mimosa',
        ]
        for bad_val in bad_values:
            user.email = bad_val
            with self.assertRaises(IntegrityError, msg=f"Invalid validation for value {bad_val}"):
                user.save()  # Guardando el modelo.

        user.email = 'mimosa@gmail.com'
        user.save()
        db_user = User.objects.get(email=user.email)
        self.assertEqual(user.id, db_user.id)

    def test_user_first_name_mandatory(self):
        """
        Testing `accounts.User` model has the `fist_name` column as not-null
        """
        user = User(
            username='jorge',
            password='123456',
            email='jzetien@gmail.com',
            last_name='zetien'
        )
        with self.assertRaises(IntegrityError):
            user.save()
        
    def test_user_last_name_mandatory(self):
        """
        Testing `accounts.User` model has the `last_name` column as not-null
        """
        user = User(
            username='zetien',
            password='123456',
            email='jzetien@gmail.com',
            first_name='jorge'
        )
        with self.assertRaises(IntegrityError):
            user.save()